/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel, Corey Gaspard

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/skyhuborg/tracker/internal/common"
	"gitlab.com/skyhuborg/tracker/internal/video"
	"gitlab.com/skyhuborg/tracker/pkg/config"

	"net/http"
	_ "net/http/pprof"
)

type Environment struct {
	Profile             bool
	Debug               bool
	DataPath            string
	ConfigFile          string
	DnnModelPath        string
	MotionDetectMinArea float64
}

type TrackerVideo struct {
	// Command line and Environment variables
	env Environment
	// Configuration settings for the tracker
	settings config.Config
	// database handle
	videoChannel chan video.VideoRecording
}

var env Environment
var BuildId string
var BuildDate string

func InitializeStore() {
	videoDir := fmt.Sprintf("%s/video", env.DataPath)
	etcDir := fmt.Sprintf("%s/etc", env.DataPath)

	if !common.Mkdir(etcDir) {
		log.Fatalf("Failed making directory: %s\n", etcDir)
	}
	if !common.Mkdir(videoDir) {
		log.Fatalf("Failed making directory: %s\n", etcDir)
	}

}

///TODO change this to do alerting with redis

// func DbOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {
// 	var (
// 		rec    db.Sensor
// 		err    error
// 		report *pb.SensorReport
// 		data   []byte
// 	)

// 	for {
// 		report = <-ch

// 		data, err = proto.Marshal(report)

// 		if err != nil {
// 			log.Printf("Failed Marshaling SensorReport: %s\n", err)
// 			continue
// 		}
// 		rec.EventId = report.EventId
// 		rec.Data = data
// 		tracker.db.AddSensorData(&rec)
// 	}
// }

func VideoAlertThread(tracker *TrackerVideo, ch chan video.VideoRecording) {

}

// func VideoUploadThread(t *Tracker) {
// 	for {
// 		var (
// 			videoEvent db.VideoEvent
// 			err        error
// 		)

// 		videoEvent, err = t.db.GetVideoEventNotUploaded()

// 		if err != nil {
// 			goto timeout
// 		}

// 		videoEvent.IsPending = true
// 		t.db.Save(videoEvent)

// 		err = t.uploadClient.Upload(videoEvent.EventId, videoEvent.Uri, videoEvent.Thumbnail)

// 		if err != nil {
// 			log.Printf("Error: Upload failed  %s\n", err)
// 			videoEvent.IsPending = false
// 			t.db.Save(videoEvent)
// 			goto timeout
// 		}
// 		videoEvent.IsUploaded = true
// 		t.db.Save(videoEvent)

// 		continue
// 	timeout:
// 		time.Sleep(5 * time.Second)
// 	}
// }

// func TrackerEventOutputThread(tracker *Tracker, ch chan event.Event) {
// 	client := tracker.client.Client

// 	for {
// 		event := <-ch
// 		ev := event.ToPb()
// 		_, err := client.AddEvent(context.Background(), ev)

// 		if err != nil {
// 			log.Printf("Error: %s\n", err)
// 		}
// 	}
// }

// func TrackerOutputThread(tracker *Tracker, ch chan *pb.SensorReport) {
// 	client := tracker.client.Client

// 	for {
// 		report := <-ch
// 		_, err := client.AddSensor(context.Background(), report)

// 		if err != nil {
// 			log.Printf("Error: %s\n", err)
// 		}
// 	}
// }

func parseArgs() {
	paramDataPath := "data-path"
	paramConfigFile := "config-file"
	paramDebug := "debug"
	paramProfile := "profile"
	paramDnnModelPath := "dnn-model-path"
	paramMotionDetectMinArea := "motion-detect-min-area"

	envDataPath := os.Getenv(paramDataPath)
	envConfigFile := os.Getenv(paramConfigFile)
	envDebug := os.Getenv(paramDebug)
	envProfile := os.Getenv(paramProfile)
	envDnnModelPath := os.Getenv(paramDnnModelPath)
	envMotionDetectMinArea := os.Getenv(paramMotionDetectMinArea)

	flag.StringVar(&env.DataPath, paramDataPath, "/skyhub/data", "path to data directory")
	flag.StringVar(&env.ConfigFile, paramConfigFile, "/skyhub/etc/tracker.yml", "path to config file")
	flag.BoolVar(&env.Debug, paramDebug, false, "Enable debugging")
	flag.BoolVar(&env.Profile, paramProfile, false, "Enable profiling")
	flag.StringVar(&env.DnnModelPath, paramDnnModelPath, "/skyhub/models", "DNN model path")
	flag.Float64Var(&env.MotionDetectMinArea, paramMotionDetectMinArea, 2000, "Minimum Area to trigger for Motion Detection")

	flag.Parse()

	if len(envDataPath) > 0 {
		env.DataPath = envDataPath
	}

	if len(envConfigFile) > 0 {
		env.ConfigFile = envConfigFile
	}

	if len(envDebug) > 0 {
		tempDebug, err2 := strconv.ParseBool(envDebug)
		if err2 == nil {
			env.Debug = bool(tempDebug)
		}
	}

	if len(envProfile) > 0 {
		tempProfile, err2 := strconv.ParseBool(envProfile)
		if err2 == nil {
			env.Profile = bool(tempProfile)
		}
	}

	if len(envDnnModelPath) > 0 {
		env.DnnModelPath = envDnnModelPath
	}

	if len(envProfile) > 0 {
		tempMinArea, err2 := strconv.ParseFloat(envMotionDetectMinArea, 64)
		if err2 == nil {
			env.MotionDetectMinArea = tempMinArea
		}
	}
}

func NewTrackerVideo(env Environment) (t TrackerVideo, err error) {

	t.env = env

	if err != nil {
		return
	}

	// Initialize the subscriber map
	// trackerdEventChannel := make(chan event.Event, 10)
	// t.state = event.NewState(&t.db)
	// t.state.RegisterChannel(trackerdEventChannel)

	// Load the local configuration for the tracker
	err = t.settings.Open(env.ConfigFile)

	if err != nil {
		log.Fatalf("Error: %s\n", err)
		return
	}

	defer t.settings.Close()

	if err != nil {
		log.Fatalf("Error: failed creating sensor.Monitor: %s", err)
		return
	}

	t.videoChannel = make(chan video.VideoRecording, 10)

	//go ControllerTrackerOutputThread(&t, controllerTrackerdChannel)
	// go TrackerOutputThread(&t, trackerdChannel)
	// go TrackerEventOutputThread(&t, trackerdEventChannel)
	// go DbOutputThread(&t, t.dbChannel)
	// go DbOutputVideoThread(&t, t.videoChannel)

	// start := time.Now()

	if len(t.settings.GetCameras()) > 0 {
		for _, c := range t.settings.GetCameras() {
			if !c.Enabled {
				continue
			}

			videoSource := video.VideoSource{
				Uri:      c.Uri,
				Username: c.Username,
				Password: c.Password,
			}

			v := video.NewVideo(env.DataPath)
			v.RegisterVideoChannel(t.videoChannel)

			// v.SetState(t.state)

			success := v.Open(&videoSource)

			if !success {
				log.Printf("Failed opening video source: %s\n", videoSource.Uri)
				return
			}

			motionDetector := video.NewMotionDetector()
			motionDetector.SetMinimumArea(env.MotionDetectMinArea)
			// motionDetector.SetState(t.state)

			/*
				dnnDetector := video.NewDnn()
				dnnDetector.SetState(t.state)
				dnnDetector.SetModelPath(env.DnnModelPath)

				v.RegisterVideoOutput(dnnDetector)
			*/
			v.RegisterVideoOutput(motionDetector)

			go v.Start()
			// go t.StartTranscoder(v)
			go v.StartProcessors()
		}
	} else {
		log.Println("No cameras detected.  Operating in Sensor only mode.")
	}
	return
}

func (t *TrackerVideo) Stop() {

	// t.client.Close()
	// t.controllerClient.Close()
	// t.db.Close()
}

func (t *TrackerVideo) Start() (bSuccess bool) {

	// log.Printf("Tracker=%s UUID=%s is Online\n", t.node_name, t.id)
	log.Printf("Video recorder started")

	// go VideoUploadThread(t)

	return
}

func main() {
	var (
		err          error
		trackerVideo TrackerVideo
		g_Shutdown   bool
	)
	common.Info.BuildId = BuildId
	common.Info.BuildDate = BuildDate

	log.Printf("Running Build ID: %s built at %s\n", common.Info.BuildId, common.Info.BuildDate)
	// load all command line args and env variables
	parseArgs()

	if env.Profile {
		go func() {
			log.Println(http.ListenAndServe(":6060", nil))
		}()
		log.Println("Profiling mode enabled.")
	}

	// creates the directory structure for the data path
	InitializeStore()

	// create our tracker instance and expose the Environment to it
	trackerVideo, err = NewTrackerVideo(env)

	if err != nil {
		log.Fatalf("Tracker video processor initialization failed: %s\n", err)
		return
	}

	for range time.Tick(60 * time.Second) {
		if g_Shutdown {
			break
		}
	}
	trackerVideo.Stop()
}
