/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package event

import (
	//pb "gitlab.com/skyhuborg/proto-trackerd-go"
	pb "gitlab.com/skyhuborg/tracker/internal/proto"
)

// Source identifies where an Event Originated from
type Source struct {
	pb.EventSource
	//	name    string     // name of the source
	//	srcType SourceType // type of the source, see SourceType
	//	sensor  string
}

func (src *Source) GetName() string {
	return src.Name
}

func (src *Source) GetType() pb.EventType {
	return src.Type
}

func (src *Source) GetSensor() string {
	return src.Sensor
}

func (src *Source) Set(name string, srcType pb.EventType) {
	src.Name = name
	src.Type = srcType
}
