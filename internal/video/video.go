/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

package video

// #cgo pkg-config: gstreamer-1.0 gstreamer-app-1.0 glib-2.0
// #cgo CFLAGS: -Wall -g -O2
// #include "gstvideo.h"
import "C"
import (
	"fmt"
	//"image"
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
	"log"
	"reflect"
	"sync"
	"unsafe"
)

type Video struct {
	this *C.sh_video_t

	src             VideoSource
	VideoOutput     []VideoProcessor
	RecordingOutput []chan VideoRecording
	state           *event.State
	finished        sync.WaitGroup
	storagePath     string
}

type VideoSource struct {
	Type     int
	Uri      string
	Username string
	Password string
	Height   int
	Width    int
	Fps      float32
	Codec    string

	DisableRecord bool
}

type VideoProcessor interface {
	Init()
	Name() string
	SetState(state *event.State)
	Run(img gocv.Mat)
	Close()
}

type VideoRecording struct {
	EventId      string
	VideoUri     string
	ThumbnailUri string
}

func NewVideo(storagePath string) *Video {
	video := Video{}
	video.storagePath = storagePath

	cStoragePath := C.CString(storagePath)

	video.this = C.sh_video_new(cStoragePath)
	C.free(unsafe.Pointer(cStoragePath))
	C.fflush(C.stdout)
	return &video
}

func (v *Video) SetState(state *event.State) {
	v.state = state
	v.state.Ref()
}

func (v *Video) RegisterVideoOutput(proc VideoProcessor) {
	v.VideoOutput = append(v.VideoOutput, proc)
}

func (v *Video) RegisterVideoChannel(ch chan VideoRecording) {
	v.RecordingOutput = append(v.RecordingOutput, ch)
}

func (v *Video) Open(src *VideoSource) bool {
	var rc C.int
	var cVideoSource C.sh_video_src_t

	v.src = *src

	cVideoSource.Type = C.int(src.Type)
	cVideoSource.uri = C.CString(src.Uri)
	defer C.free(unsafe.Pointer(cVideoSource.uri))

	cVideoSource.username = C.CString(src.Username)
	defer C.free(unsafe.Pointer(cVideoSource.username))

	cVideoSource.password = C.CString(src.Password)
	defer C.free(unsafe.Pointer(cVideoSource.password))

	cVideoSource.height = C.int(src.Height)
	cVideoSource.width = C.int(src.Width)
	cVideoSource.fps = C.float(src.Fps)
	cVideoSource.codec = C.CString(src.Codec)
	defer C.free(unsafe.Pointer(cVideoSource.codec))

	rc = C.sh_video_open(v.this, &cVideoSource)

	if rc != 0 {
		return false
	}

	return true
}

func (v *Video) Start() bool {
	var rc C.int

	rc = C.sh_video_start(v.this)

	if rc != 0 {
		return false
	}

	return true
}

func (v *Video) Stop() bool {
	var rc C.int

	rc = C.sh_video_stop(v.this)

	if rc != 0 {
		return false
	}

	return true
}

func (v *Video) GetIsRecording() bool {
	var rc C.int

	rc = C.sh_video_get_is_recording(v.this)

	if rc == 1 {
		return true
	}

	return false
}

func (v *Video) generateThumbnail(ev *event.Event, img *gocv.Mat) (success bool) {
	defer v.finished.Done()

	if img.Empty() {
		log.Printf("Thumbnail for event %s is empty, ignoring\n", ev.GetId())
		return
	}

	thumbnailUri := fmt.Sprintf("%s/thumbnail/%s.webp", v.storagePath, ev.GetId())
	options := []int{gocv.IMWriteWebpQuality, 80}
	success = gocv.IMWriteWithParams(thumbnailUri, *img, options)

	if success == false {
		log.Printf("Failed writing thumbnail for event '%s'\n", ev.GetId())
	}

	return
}

func (v *Video) StartProcessors() {
	output := ""
	for _, processor := range v.VideoOutput {
		processor.Init()
		output += fmt.Sprintf("%s ", processor.Name())
	}

	log.Printf("Video: Starting with the following Processors: [ %s]\n", output)

	v.IsSinkPlaying()

	// get the structure
	for {
		var (
			ev   *event.Event
			dims []int
			err  error
			img  gocv.Mat
		)
		ev = v.state.GetEvent()

		frame := v.GetSinkFrame()

		//img, err = gocv.NewMatFromBytes(frame.Height, frame.Width, gocv.MatTypeCV8UC3, frame.Data) // 16
		img, err = gocv.NewMatFromBytes(frame.Height, frame.Width, gocv.MatTypeCV8UC1, frame.Data) // 16

		if err != nil {
			v.ReleaseFrame(&frame)
			continue
		}

		if ev.GetInProgress() {
			if !v.GetIsRecording() {
				v.StartRecording(ev.GetId())
				// increment the wait group so the thumbnail
				// always gets generated before we stop recording
				v.finished.Add(1)
				go v.generateThumbnail(ev, &img)
			}
		} else {
			if ev.GetIsComplete() == false {
				if v.GetIsRecording() {
					recording := v.StopRecording()
					_ = recording
					ev.Done()
				}
			}
		}

		if img.Empty() {
			goto cleanup
		}

		if img.Cols() == 0 || img.Rows() == 0 {
			goto cleanup
		}

		dims = img.Size()

		if dims[0] <= 2 || dims[1] <= 2 {
			goto cleanup
		}

		for _, processor := range v.VideoOutput {
			processor.Run(img)
		}
	cleanup:
		v.ReleaseFrame(&frame)
		img.Close()
	}

	return
}

func (v *Video) StartRecording(EventUuid string) bool {
	var rc C.int
	var cEventUuid *C.char

	cEventUuid = C.CString(EventUuid)

	rc = C.sh_video_start_recording(v.this, cEventUuid)

	C.free(unsafe.Pointer(cEventUuid))
	if rc != 0 {
		return false
	}

	return true
}

func (v *Video) StopRecording() VideoRecording {
	var cRecording *C.sh_video_recording_t
	var recording VideoRecording

	cRecording = C.sh_video_stop_recording(v.this)

	if cRecording == nil {
		return recording
	}

	recording.EventId = C.GoString(cRecording.event_id)
	recording.VideoUri = C.GoString(cRecording.video_uri)
	recording.ThumbnailUri = C.GoString(cRecording.thumbnail_uri)

	C.sh_video_recording_free(cRecording)

	for _, ch := range v.RecordingOutput {
		ch <- recording
	}
	// call to sh_video_recording_free

	return recording
}

func (v *Video) IsSinkPlaying() bool {
	C.sh_video_is_sink_ready(v.this)
	return true
}

type Frame struct {
	this     C.sh_video_frame_t
	Data     []byte
	DataSize int
	Height   int
	Width    int
}

var sample_height C.int
var sample_width C.int

func (v *Video) GetSinkFrame() Frame {
	var rc C.int
	const maxLen = 0x7fffffff
	var framePtr C.sh_video_frame_t

	rc = C.sh_video_get_sink_frame(v.this, &framePtr)

	if rc != 0 {
		return Frame{}
	}
	var bytes []byte

	p := uintptr(unsafe.Pointer(framePtr.data))

	sh := (*reflect.SliceHeader)(unsafe.Pointer(&bytes))
	sh.Data = p
	sh.Len = int(framePtr.dataSize)
	sh.Cap = int(framePtr.dataSize)

	//bytes := C.GoBytes(unsafe.Pointer(framePtr.data), framePtr.dataSize)

	frame := Frame{
		Data:     bytes,
		this:     framePtr,
		DataSize: int(framePtr.dataSize),
		Height:   int(framePtr.height),
		Width:    int(framePtr.width),
	}

	return frame
}

func (v *Video) ReleaseFrame(frame *Frame) {
	if frame != nil {
		C.sh_video_release_frame(&frame.this)
	}
}

func (v *Video) Transcode(input string, output string) bool {
	var rc C.int
	var cIn *C.char
	var cOut *C.char
	var success bool = true

	cIn = C.CString(input)
	cOut = C.CString(output)

	rc = C.sh_video_transcode(v.this, cIn, cOut)

	if rc != 0 {
		success = false
	}

	C.free(unsafe.Pointer(cIn))
	C.free(unsafe.Pointer(cOut))

	return success
}

func (v *Video) Free() {
	C.sh_video_free(v.this)
}
